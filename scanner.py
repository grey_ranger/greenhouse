from logging.handlers import RotatingFileHandler
import datetime
import io
import json
import logging
import os
import paho.mqtt.client as mqtt
import smbus2
import time

mqttBroker = '192.168.1.144'
token = os.environ.get("IOT_TOKEN", "")
base_path = '/home/stuart/greenhouse'
logging.basicConfig(
        handlers=[
            RotatingFileHandler(os.path.join(base_path, 'scanner.log'), maxBytes=100000, backupCount=10),
            logging.StreamHandler()
        ],
        level=logging.DEBUG,
        format="%(asctime)s %(levelname)s %(funcName)s:%(lineno)d %(message)s",
        datefmt='%Y-%m-%dT%H:%M:%S'
)
logger = logging.getLogger()

# Get I2C Bus
bus = smbus2.SMBus(1)

# Initialize MQTT client
client = mqtt.Client()
client.username_pw_set(token)
client.connect(mqttBroker, 1883, 60)
client.loop_start()

def read_temp_humidity():
    # Modified from  https://www.controleverything.com/content/Temperature?sku=HCPA-5V-U3_I2CS#tabs-0-product_ta>
    # HCPA_5V_U3 address, 0x28(40)
    # Send start command, 0x80(128)
    bus.write_byte(0x28, 0x80)

    time.sleep(0.5)

    # HCPA_5V_U3 address, 0x28(40)
    # Read data back, 4 bytes
    # humidity msb, humidity lsb, cTemp msb, cTemp lsb
    result = bus.read_i2c_block_data(0x28, 1, 4)

    # Convert the data to 14-bits and add to dictionary
    temp_C = (((result[2] * 256) + (result[3] & 0xFC)) / 4) / 16384.0 * 165.0 - 40.0
    return {
        'rel_humidity': (((result[0] & 0x3F) * 256) + result[1]) / 16384.0 * 100.0,
        'temp_C': temp_C,
        'temp_F': (temp_C * 1.8) + 32,
        'ts': int(datetime.datetime.now().timestamp() * 1000)
    }


while True:
    time.sleep(10)
    try:
        data = read_temp_humidity()
    except Exception as e:
        logger.error(str(e))
        continue
    if not data:
        continue
    logger.debug(str(data))
    client.publish("v1/devices/me/telemetry", json.dumps(data))
