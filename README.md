# Greenhouse

Automation for greenhouse monitoring

## Description

Operate an arduino to read temperature, humidity, pressure sensors. Log output through serial to a Raspberry Pi. The Pi stores results in an SQLite table, to allow for easy access and analysis. The Pi and Arduino are configured for headless operation, though the Pi is accessible through SSH for updates.

## Installation

Follow [NCD Example](https://github.com/ControlEverythingCommunity/HCPA-5V-U3/blob/master/Python/HCPA_5V_U3.py) for the device.
Use [smbus2](https://www.engineersgarage.com/articles-raspberry-pi-i2c-bus-pins-smbus-smbus2-python/) for I2C communication.

`i2cdetect -y 1` gives a list of items on the I2C bus.

Set up the I2C bus to be read/writable to all users:

`sudo nano /etc/rc.local`

add the line

`chmod 777 /dev/i2c-1`
