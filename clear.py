import sqlite3
con = sqlite3.connect('greenhouse.db')

with con:
    con.execute("DROP TABLE IF EXISTS greenhouse")
