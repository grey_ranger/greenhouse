from flask import Flask
import sqlite3
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

app = Flask(__name__)

@app.route('/')
def index():
    con = sqlite3.connect('/home/stuart/greenhouse/greenhouse.db')
    df = pd.read_sql("SELECT * FROM greenhouse ORDER BY timestamp DESC LIMIT 1000", con)
    fig = px.line(df, x='timestamp', y='temp_F')
    return fig.to_html()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8050, threaded=True)
