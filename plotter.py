# Importing Libraries
from suntime import Sun
import datetime
import io
import json
import pandas as pd
import serial
import sqlite3
import seaborn as sns
import time
import plotly.graph_objects as go
con = sqlite3.connect(r'C:\users\teres\Documents\Arduino\greenhouse\greenhouse.db')
df = pd.read_sql("SELECT * FROM greenhouse", con)

def datetime_to_time(x):
    return x.hour * 3600 + x.minute * 60 + x.second

df['date'] = df['timestamp'].apply(lambda x: pd.to_datetime(x).date())
df['time_of_day'] = df['timestamp'].apply(lambda x: pd.to_datetime(x).time())
df['time'] = df.time_of_day.apply(datetime_to_time)
df['time_of_day'] = df['time_of_day'].apply(lambda x: x.strftime('%H:%M:%S'))

downsample = 10

latitude = 43.6135002
longitude = -116.2034505
sun = Sun(latitude, longitude)
sunrises = []
sunsets = []

x = 'time'
y = 'temp_F'
palette = sns.color_palette('husl')
fig = go.Figure()
fig.update_layout(
    title="Greenhouse Temperature by Day",
    yaxis_title="Temp [°F]",
    xaxis_title="Time of Day",
)
for (ind, grp), color in zip(df.groupby('date'), palette):
    color='rgba({}, .4)'.format(", ".join([str(int(x*255)) for x in color]))
    fig.add_trace(
        go.Scatter(
            x=grp[x], 
            y=grp[y],
            marker_color=color,
            name=str(ind.strftime('%b %d')),
            customdata=grp['time_of_day'],
            hovertemplate = '%{customdata}<br>%{y}°F<br>'
        )
    )
    smoothed = grp.iloc[::downsample, :]
    smoothed['smoothed_temp_F'] = smoothed['temp_F'].rolling(6, min_periods=1, center=True).mean()

    fig.add_trace(
        go.Scatter(
            x=smoothed[x], 
            y=smoothed['smoothed_temp_F'],
            marker_color=color.replace('.4', '.8'),
            name='{} Avg'.format(ind.strftime('%b %d')),
            customdata=smoothed['time_of_day'],
            hovertemplate = '%{customdata}<br>%{y}°F<br>'
        )
    )

    fig.add_vline(x=grp[x][grp[y].idxmin()], line_dash="dash", line_color=color)
    fig.add_vline(x=grp[x][grp[y].idxmax()], line_dash="dash", line_color=color)
    sunrise = sun.get_local_sunrise_time(ind)
    sunset = sun.get_local_sunset_time(ind)
    sunrise_temp = grp[y].loc[(grp[x] - datetime_to_time(sunrise)).abs().idxmin()]
    sunset_temp = grp[y].loc[(grp[x] - datetime_to_time(sunset)).abs().idxmin()]
    sunrises.append((sunrise, sunrise_temp, ind))
    sunsets.append((sunset, sunset_temp, ind))

sunrises = pd.DataFrame(sunrises, columns=['sunrise', 'sunrise_temp', 'date']).sort_values('date')
sunrises['time'] = sunrises['sunrise'].apply(datetime_to_time)
fig.add_trace(
    go.Scatter(
        x=sunrises['time'],
        y=sunrises['sunrise_temp'],
        mode='lines+markers',
        marker_color="gold",
        hoverinfo='x,y',
        name='Sunrise',
        customdata=sunrises['sunrise'],
        hovertemplate = '%{customdata}<br>%{y}°F<br>'
    )
)
sunsets = pd.DataFrame(sunsets, columns=['sunset', 'sunset_temp', 'date']).sort_values('date')
sunsets['time'] = sunsets['sunset'].apply(datetime_to_time)
fig.add_trace(
    go.Scatter(
        x=sunsets['time'],
        y=sunsets['sunset_temp'],
        mode='lines+markers',
        marker_color="goldenrod",
        hoverinfo='x,y',
        name='Sunset',
        customdata=sunsets['sunset'],
        hovertemplate = '%{customdata}<br>%{y}°F<br>'
    )
)

fig.update_layout(
    xaxis = dict(
        tickmode = 'array',
        tickvals = [x * 3600 for x in range(25)],
        ticktext = ["12:00am"] + ["{}:00am".format(x) for x in range(1, 12)] + ["12:00pm"] + ["{}:00pm".format(x) for x in range(1, 13)]
    )
)
fig.update_layout(hovermode="x")
fig.show()