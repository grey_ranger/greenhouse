#! /bin/bash
source /etc/profile
[[ `pgrep -f "scanner"` ]] && echo "Scanner Running" || /home/stuart/miniforge3/envs/greenhouse/bin/python /home/stuart/greenhouse/scanner.py > /dev/null 2>&1
[[ `pgrep -f "dashboard"` ]] && echo "Dashboard Running" || /home/stuart/miniforge3/envs/greenhouse/bin/python /home/stuart/greenhouse/dashboard.py > /dev/null 2>&1
